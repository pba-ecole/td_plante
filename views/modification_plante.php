<!DOCTYPE html>
<html lang="en" dir="ltr">
    <?php include "../templates/header.php"; ?>
    <body>
        <?php
            require("../config/connexion.php");

            $noPlante = $_GET["noPlante"];
            $regions = $connexion->query("SELECT * FROM region");
            $regions->setFetchMode(PDO::FETCH_OBJ);
            $serres = $connexion->query("SELECT * FROM serre");
            $serres->setFetchMode(PDO::FETCH_OBJ);
            $selectPlante = $connexion->query("SELECT * FROM plante WHERE noplante=" . $noPlante . "");
            $selectPlante->setFetchMode(PDO::FETCH_OBJ);
            $plante = $selectPlante->fetch();
            $selectSerre = $connexion->query("SELECT * FROM emplacement WHERE noplante=" . $noPlante . "");
            $selectSerre->setFetchMode(PDO::FETCH_OBJ);
            $serreSelect = $selectSerre->fetch();
        ?>

        <h1>Modifier une plante</h1>
        <hr>

        <form action="../controllers/modifier_plante.php" method="post">
            <label for="nbrIdPlante">Id de la plante : </label>
            <input type="number" name="nbrIdPlante" class="form-control" value="<?= $plante->noplante; ?>" readonly>

            <label for="txtNomPlante">Nom de la plante : </label>
            <input type="text" name="txtNomPlante" class="form-control" value="<?= $plante->nomplante; ?>">

            <label for="slctNoRegion">Région : </label>
            <select class="form-select" name="slctNoRegion">
                <?php echo $plante->noregion ?>
                <option value="0">Sélectionner une région</option>
                <?php foreach ($regions as $region): ?>
                    <?php if ($region->noregion == $plante->noregion): ?>
                        <option value="<?= $region->noregion ?>" selected><?= $region->nomregion; ?></option>
                    <?php else: ?>
                        <option value="<?= $region->noregion; ?>"><?= $region->nomregion; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>

            <label for="slctNoSerre">Serre : </label>
            <select class="form-select" name="slctNoSerre">
                <option value="0" selected>Sélectionner une Serre</option>
                <?php foreach ($serres as $serre): ?>
                    <?php if ($serreSelect->noserre == $serre->noserre): ?>
                        <option value="<?= $serre->noserre ?>" selected><?= $serre->nomserre; ?></option>
                    <?php else: ?>
                        <option value="<?= $serre->noserre; ?>"><?= $serre->nomserre; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>

            <div class="d-flex p-3 m-3 justify-content-center">
                <a href="../index.php" class="btn btn-danger m-3">Annuler</a>
                <button type="submit" name="btnValider" class="btn btn-primary m-3">Valider</button>
            </div>
        </form>
    </body>
</html>
