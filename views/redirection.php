<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <?php include "../templates/header.php"; ?>
    <body>
        <h1><?php echo $_SESSION["nomUtilisateur"]; ?></h1>
        <?php print_r($_SESSION); ?>

        <form action="../utils/clear-session.php" method="post">
            <input type="submit" value="Détruire la session">
        </form>
    <?php include "../templates/footer.php"; ?>
    </body>
</html>
