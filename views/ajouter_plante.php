<!DOCTYPE html>
<html lang="en" dir="ltr">
    <?php include "../templates/header.php"; ?>
    <body>
        <?php
            require_once("../config/connexion.php");
            $regions = $connexion->query("SELECT * FROM region");
            $regions->setFetchMode(PDO::FETCH_OBJ);
            $serres = $connexion->query("SELECT * FROM serre");
            $serres->setFetchMode(PDO::FETCH_OBJ);
        ?>

        <h1>Ajouter une plante</h1>
        <hr>

        <form action="../controllers/inserer_plante.php" method="post">
            <label for="nbrIdPlante">Id de la plante : </label>
            <input type="number" name="nbrIdPlante" class="form-control">

            <label for="txtNomPlante">Nom de la plante : </label>
            <input type="text" name="txtNomPlante" class="form-control">

            <label for="slctNoRegion">Région : </label>
            <select class="form-select" name="slctNoRegion">
                <option value="0" selected>Sélectionner une région</option>
                <?php foreach ($regions as $region): ?>
                    <option value="<?= $region->noregion; ?>"><?= $region->nomregion; ?></option>
                <?php endforeach; ?>
            </select>

            <label for="slctNoSerre">Serre : </label>
            <select class="form-select" name="slctNoSerre">
                <option value="0" selected>Sélectionner une Serre</option>
                <?php foreach ($serres as $serre): ?>
                    <option value="<?= $serre->noserre; ?>"><?= $serre->nomserre; ?></option>
                <?php endforeach; ?>
            </select>

            <div class="d-flex p-3 m-3 justify-content-center">
                <a href="index.php" class="btn btn-danger m-3">Annuler</a>
                <button type="submit" name="btnValider" class="btn btn-primary m-3">Valider</button>
            </div>
        </form>
    </body>
</html>
