<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <?php include "../templates/header.php"; ?>
    <body>
        <?php
            require_once("../config/connexion.php");

            $selectPlante = $connexion->query("SELECT * FROM plante WHERE noplante=" . $_GET["noPlante"]);
            $selectPlante->setFetchMode(PDO::FETCH_OBJ);
            $unePlante = $selectPlante->fetch();

            $selectRegion = $connexion->query("SELECT * FROM region WHERE noregion=" . $unePlante->noregion);
            $selectRegion->setFetchMode(PDO::FETCH_OBJ);
            $uneRegion = $selectRegion->fetch();

            $selectSerre = $connexion->query(
                "SELECT nomserre
                FROM serre se
                INNER JOIN emplacement em ON(se.noserre=em.noserre)
                INNER JOIN plante pl ON(em.noplante=pl.noplante)
                WHERE pl.noplante=" . $unePlante->noplante
            );
            $selectSerre->setFetchMode(PDO::FETCH_OBJ);
            $uneSerre = $selectSerre->fetch();
        ?>

        <?php if ($unePlante): ?>
            <ul class="list-group">
                <li class="list-group-item active"><?= $unePlante->nomplante; ?></li>
                <li class="list-group-item"><?= $unePlante->noplante; ?></li>
                <li class="list-group-item"><?= $uneRegion->nomregion; ?></li>
                <li class="list-group-item"><?= $uneSerre->nomserre; ?></li>
            </ul>
            <a href="../controllers/suppression_plante.php?noPlante=<?= $unePlante->noplante ?>" class="btn btn-danger">Supprimer la plante</a>
            <a href="../views/modification_plante.php?noPlante=<?= $unePlante->noplante ?>" class="btn btn-primary">Modifier la plante</a>
        <?php include "../templates/footer.php"; ?>
        <?php else: ?>
            <h4>Aucun résultat</h4>
        <?php endif; ?>


    </body>
</html>
