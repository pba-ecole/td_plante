<?php
    require_once("../config/connexion.php");

    try {
        $noPlante = $_GET["noPlante"];

        $requete1 = "DELETE FROM emplacement WHERE noplante=" . $noPlante . "";
        $connexion->exec($requete1);
        $requete = "DELETE FROM plante WHERE noplante=" . $noPlante . "";
        $connexion->exec($requete);
        header("Location:../index.php");
    } catch (Exception $e) {
        echo "Une erreur est survenue lors de la suppression\n";
        echo $e;
    }

?>
