<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <?php include "templates/header.php"; ?>

    <body>
        <?php if (!isset($_POST["btnValider"])): ?>
            <form action="" method="post">
                <label for="txtNom" class="form-label">Nom :</label>
                <input type="text" name="txtNom" class="form-control" placeholder="Entrez votre nom...">
                <input type="submit" name="btnValider" class="btn btn-primary">
            </form>
        <?php else: ?>
            <h1>Bonjour <?php echo $_POST["txtNom"]; ?></h1>
            <?php $_SESSION["nomUtilisateur"] = $_POST["txtNom"]; ?>
            <a href="views/redirection.php">Redirection</a>
            <hr>
            <?php
                require_once("config/connexion.php");
                $select = $connexion->query("select * from plante");
                $select->setFetchMode(PDO::FETCH_OBJ);
            ?>

            <h3>Liste des plantes</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Identifiant</th>
                        <th scope="col">Nom</th>
                    </tr>
                </thead>

                <tbody>
                    <?php while ($plante = $select->fetch()): ?>
                        <tr>
                            <td><?= $plante->noplante; ?></td>
                            <td><a href="views/lister_une_plante.php?noPlante=<?= $plante->noplante ?>"><?= $plante->nomplante; ?></a></td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
            <a href="views/ajouter_plante.php" class="btn btn-primary">Ajouter une plante</a>
        <?php endif; ?>
    <?php include "templates/footer.php"; ?>
    </body>
</html>
